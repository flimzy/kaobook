FROM golang:1.14-buster

RUN apt-get update && apt-get install -y \
    biber \
    imagemagick \
    pdfgrep \
    pdftk \
    texlive-bibtex-extra \
    texlive-fonts-extra \
    texlive-latex-base \
    texlive-latex-extra \
    texlive-latex-recommended \
    texlive-science \
    qpdf \
    xindy \
&& rm -rf /var/lib/apt/lists/*
RUN sed -ie '/PDF/s/\"none\"/\"read|write\"/' /etc/ImageMagick-6/policy.xml